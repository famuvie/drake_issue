Drake warning in targets using `file_in()` and `here::here()`
=============================================================

This project contains a minimal reproducible example of the issue.

There is a target `mtcars_ok` that reads the file `data/mtcars.csv`
using `read.csv(file_in(here("data/mtcars.csv")))`. This works well.

However, if the data is read alternatively using separate path
components as in `mtcars_warn = read.csv(file_in(here("data", "mtcars.csv")))`,
the project builds with a warning about missing input files.

The issue also happens with `file_out()`, and `knitr_in/out()`.

To reproduce, clone the project with

```
git clone git@forgemia.inra.fr:famuvie/drake_issue.git
```

open an R session within the `drake_issue` directory and do:

```
library(drake)
r_make()
```
